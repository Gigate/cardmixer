import 'package:card_mixer/routing/routing_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Card Mixer'),),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_arrow,
        children: [
          SpeedDialChild(
            onTap: () async {

            },
            child: Icon(Icons.create),
            label: "Lobby erstellen",),
          SpeedDialChild(
            onTap: () async {
              Navigator.of(context).pushNamed(playscreenroute);
            },
            child: Icon(Icons.arrow_forward),
            label: "Lobby beitreten"),
        ],
      ),
    );
  }

}