import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class PlayScreen extends StatefulWidget {
  @override
  _PlayScreenState createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  bool roundStart = true;

  List<String> handCards = ["13", "2", "1-7", "11"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Spielbrett")),

      body: Column(
        children: <Widget>[
          if (roundStart) Center(
            child: Container(
              width: 100,
              height: 200,
              decoration: BoxDecoration(
                border: Border.all(style: BorderStyle.solid)
              ),
              child: Center(child: Text("Partner geben")),
            ),
          ),
          if(!roundStart) Row(),
          Expanded(
            child: GridView.count(
              crossAxisCount: 3,

              children: handCards.map((card) {
                return LongPressDraggable(
                  child: Card(
                    elevation: 5,
                    child: Image.asset("assets/DogCard_$card.png"),
                  ),
                  feedback: Card(
                    elevation: 20,
                    child: Image(image: AssetImage("dog/DogCard_$card.png")),
                  ),

                );
              }).toList(),
            ),
          )
        ],
      ),

    );
  }
}