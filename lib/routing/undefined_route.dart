import 'package:flutter/material.dart';

class UndefinedView extends StatelessWidget {
  final String name;

  UndefinedView(this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home View"),
      ),
      body: Center(
        child: Text("Route for $name is not defined!"),
      ),
    );
  }

}