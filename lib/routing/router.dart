import 'package:card_mixer/routing/undefined_route.dart';
import 'package:card_mixer/screens/home.dart';
import 'package:card_mixer/screens/playscreen.dart';
import 'package:flutter/material.dart';

import './routing_constants.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case homeRoute:
      return MaterialPageRoute(
        builder: (context) => HomeScreen());

    case playscreenroute:
      return MaterialPageRoute(
        builder: (context) => PlayScreen());

    default:
      return MaterialPageRoute(
          builder: (context) => UndefinedView(settings.name));
  }
}
